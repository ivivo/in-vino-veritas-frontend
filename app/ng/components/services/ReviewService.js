/**
 * ReviewService
 * Common functions used when displaying and modifying recommendations for wines.
 */
app.factory('ReviewService', ['$http', 'config', 'HttpService',
    function ($http, config, HttpService) {

        var errors;
        var success;

        function clearErrors() {
            errors = null;
            success = null;
        }
        
        var updateRecommendations = function(wine){
            HttpService.get({
                url: '/api/wines/' + wine._id + '/recommendations',
                success: function (data) {
                    wine.recommendations = data;
                }
            });
        };

        var toggleReviewsSize = function(wineID){
            var reviews = '#' + wineID + '-reviews';
            if ($(reviews).hasClass('col-md-9')) {
                $(reviews).addClass('col-md-12');
                $(reviews).removeClass('col-md-9');
            }
            else {
                $(reviews).addClass('col-md-9');
                $(reviews).removeClass('col-md-12');
            }
        };

        return{
            review: {},

            toggleReviewsSize: toggleReviewsSize,

            updateRecommendations: updateRecommendations,

            deleteReview: function(wine,review){
                clearErrors();
                var data = {
                    url: '/api/wines/' + wine._id + '/recommendations/' + review._id,
                    error: function(errors) {
                        this.errors = errors;
                    },
                    success: function(message){
                        this.success = message;
                        console.log('Deleted recommendationID ' + review._id + ' for wineID: ' + wine._id);
                        updateRecommendations(wine);
                    }
                };
                HttpService.delete(data);
            },

            rangeStars: function(stars){
                var range = [];
                for (var i = 1; i <= stars; i++) {
                    range.push(i);
                }
                return range;
            },

            avgStars: function(wine){
                if (wine == undefined || wine.recommendations == undefined){
                    return 0;}
                var countStars = 0;
                var countRecommendations = 0;
                var avgStars = 0;
                var range = [];
                for (i = 0; i < wine.recommendations.length; i++) {
                    countStars += wine.recommendations[i].stars;
                    countRecommendations += 1;
                }
                avgStars = countStars / countRecommendations;
                for (i = 1; i <= Math.round(avgStars); i++) {
                    range.push(i);
                }
                return range;
            }

        };

    }]);