/**
 * HttpService
 * Manages all CRUD connections to the backend.
 * Handles GET, POST, PUT and DELETE requests.
 * Provides the caller with uniformed information about success and error of $http functions.
 * Provides uniform login for $http requests.
 */

app.factory('HttpService', ['$http', 'config', 'AuthService',
    function ($http, config, AuthService) {

        var logger = new L4JS("HttpService");
        var backendUrl = config.backendUrl;

        var promiseSuccessHandler = function (response, config) {
            logger.debug(config.method + '-Request: "' + config.url + '" returns the following:', response);
            if (response.data == null) return;
            var message = response.data.message;
            if (typeof config.success === 'function')
                config.success(message);
        };

        var promiseErrorHandler = function (response, config) {
            logger.warn(config.method + ' resulted in errors:', response);
            var errors = [];
            var responseErrors = response.data.errors;
            if (response.data.name == "ValidationError") {
                errors = responseErrors; 
            } else {
                for (key in responseErrors) {
                    errors.push({
                        name: key,
                        value: responseErrors[key].message
                    })
                }
            }
            logger.debug(errors);
            if (typeof config.error === 'function')
                config.error(errors);
        };

        return {
            get: function (config) {
                
                var request = {
                    method: 'GET',
                    url: backendUrl + config.url,
                    headers: {
                        "authorization": AuthService.token
                    }
                };

                if (typeof config.params != 'undefined')
                    request.params = config.params;

                logger.debug('GET-Request: "' + config.url + '" pending...');
                $http(request)
                    .success(function (data) {
                        logger.debug('GET-Request: "' + config.url + '" returns the following:', data);
                        if (typeof config.success === 'function')
                            config.success(data);
                    })
                    .error(function (data) {
                        if (typeof config.error === 'function')
                            config.error(data);
                    });
            },
            post: function (config) {

                var request = {
                    method: "POST",
                    url: backendUrl + config.url,
                    headers: {
                        "authorization": AuthService.token
                    },
                    data: config.data
                };
                config.method = 'POST';
                
                logger.debug('POST-Request: "' + config.url + '" pending... posting the following:', request);
                $http(request)
                    .then(function (response) {
                        promiseSuccessHandler(response, config);
                    }, function (response) {
                        promiseErrorHandler(response, config);
                    });
            },
            put: function (config) {

                var request = {
                    method: "PUT",
                    url: backendUrl + config.url,
                    headers: {
                        "authorization": AuthService.token
                    },
                    data: config.data
                };
                config.method = 'PUT';

                logger.debug('PUT-Request: "' + config.url + '" pending... putting the following:', request);
                $http(request)
                    .then(function (response) {
                        promiseSuccessHandler(response, config);
                    }, function (response) {
                        promiseErrorHandler(response, config);
                    });
            },
            delete: function(config){

                var request = {
                    method: "DELETE",
                    url: backendUrl + config.url,
                    headers: {
                        "authorization": AuthService.token
                    },
                    data: config.data
                }
                logger.debug('DELETE-Request: "' + config.url + '" pending... deleting the following:', request);
                $http(request)
                    .then(function (response) {
                        promiseSuccessHandler(response, config);
                    }, function (response) {
                        promiseErrorHandler(response, config);

                    });
            }
        }
    }]);