/**
 * AuthService
 * Used to sign in, register and logout a user from the system.
 * Provides user based information like status of login, and user token. *
 */
app.factory('AuthService', ['$http', 'config',
    function ($http, config) {

        var log = new L4JS("AuthService");
        var backendUrl = config.backendUrl;

        var info = {

            /**
             * Registers a user with given username and passwort
             * @param username
             * @param password
             * @param callback: Function that will be called after registration is completed
             */
            register: function (username, password, callback) {
                log.debug("Registering with username " + username);
                $http.post(backendUrl + "/register", {
                    username: username,
                    password: password
                }).then(function (response) {
                    //success
                    info.token = "JWT " + response.data.token;
                    info.isLoggedIn = true;
                    callback();
                }, function (response) {
                    //error
                    log.debug(response);
                    info.token = "";
                });
            },

            /**
             * Sign in for a user with given username and password.
             * @param username
             * @param password
             * @param callback: Function called, in case of successful login.
             * @param unauthorizedCallback: Function called in case of unsuccessful login.
             */
            login: function (username, password, callback, unauthorizedCallback) {
                log.debug("Logging in with username " + username);
                $http.post(backendUrl + "/login", {
                    username: username,
                    password: password
                }).then(function (response) {
                    //success
                    log.info(response);
                    info.token = "JWT " + response.data.token;
                    info.isAdmin = response.data.user.isAdmin;
                    info.isLoggedIn = true;
                    callback();
                }, function (response) {
                    //error
                    log.debug(response);
                    info.token = "";
                    info.isLoggedIn = false;
                    if (response.status == 401){
                        unauthorizedCallback();
                    }
                });
            },

            /**
             * Function that logs a user out of the system and destroy the token
             * @param callback
             */
            logout: function (callback) {
                log.debug("Logging user out. Destroying token.");
                info.isLoggedIn = false;
                info.token = "";
                callback();
            },

            isLoggedIn: false,
            token: "",
            isAdmin: false,
            getToken: function() {
                return info.token;
            }

        };

        return info;

    }]);