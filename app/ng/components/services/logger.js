/**
 * Simple self-written logger to provide SEBA project with different logging styles.
 * @param file
 * @constructor
 */

L4JS = function(file) {

    this.file = file;

    this.levels = {
        "ALL" : ["INFO", "DEBUG", "WARN"],
        "INFO" : ["INFO"],
        "DEBUG" : ["DEBUG", "WARN"],
        "WARN" : ["WARN"]
    };

    this.performMessage = function(level, message, object) {

        var color = "green";

        switch (level) {
            case "DEBUG":
                color = "blue";
                break;
            case "WARN":
                color = "red";
                break;
            case "INFO":
                color = "green";
                break;
        }

        if (typeof message == 'object') {
            object = message;
            message = "Object follows:";
        }

        var fullMessage = "%c" + "" + level + " | " + this.file + " | " + message;
        var coloring = "color:" + color + ";";

        console.log(fullMessage, coloring);
        if (typeof object === "object") {
            console.log(object);
            return;
        }

    }

    this.log = function(level, message, object) {
        if (this.levels[level].indexOf(level) != -1) {
            this.performMessage(level, message, object);
        }
    }

    this.level = "INFO";
}

L4JS.prototype.setLevel = function(level) {
    this.level = level;
}
L4JS.prototype.debug = function(message, object) {
    this.log("DEBUG", message, object);
}
L4JS.prototype.warn = function(message, object) {
    this.log("WARN", message, object);
}
L4JS.prototype.info = function(message, object) {
    this.log("INFO", message, object);
}

var Logger = new L4JS();