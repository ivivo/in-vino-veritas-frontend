app.controller('mainController',
    ['$scope', 'AuthService', '$interval', '$location',
        function ($scope, AuthService, $interval, $location) {

            var main = this;

            var log = new L4JS("mainController");

            main.isLoggedIn = false;
            main.token = "token";

            // provides the page with information about login status of the user
            $scope.$watch(function () { return AuthService.isLoggedIn }, function (newVal, oldVal) {
                main.isLoggedIn = AuthService.isLoggedIn;
            }, true);

            $scope.$watch(function () { return AuthService.isAdmin }, function (newVal, oldVal) {
                main.isAdmin = AuthService.isAdmin;
            }, true);

            main.logout = function() {
                AuthService.logout(function(){
                    $location.path($location.path());
                });
            }

        }]);