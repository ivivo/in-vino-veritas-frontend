app.config([ '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl : 	'views/landing/landing.html',
            controller : 	'landingController',
            controllerAs : 'c',
            activeUrl : 	'/home',
            authenticate : false
        })
        .state('discover', {
            url: '/discover',
            templateUrl : 	'views/discover/discover.html',
            controller : 	'discoverController',
            controllerAs : 'c',
            activeUrl : 	'/discover',
            authenticate : false
        })
        .state('discoverTag', {
            url: '/discover/{tag}',
            templateUrl : 	'views/discover/discover.html',
            controller : 	'discoverController',
            controllerAs : 'c',
            activeUrl : 	'/discover',
            authenticate : false
        })
        .state('registration', {
            url: '/registration',
            templateUrl : 	'views/registration/registration.html',
            controller : 	'registrationController',
            controllerAs : 'c',
            activeUrl : 	'/registration',
            authenticate : false
        })
        .state('login', {
            url: '/login',
            templateUrl : 	'views/login/login.html',
            controller : 	'loginController',
            controllerAs : 'c',
            activeUrl : 	'/login',
            authenticate : false
        })
        .state('contact', {
            url: '/contact',
            templateUrl : 	'views/contact/contact.html',
            /**controller : 	'contactController',
            controllerAs : 'c',*/
            activeUrl : 	'/contact',
            authenticate : false
        })

        // forms
        .state('formWine', {
            url: '/form/wine',
            templateUrl : 	'views/wine/wine.html',
            controller : 	'wineFormController',
            controllerAs : 'c',
            activeUrl : 	'/form/wine',
            authenticate : true
        })
        .state('formMaker', {
            url: '/form/maker',
            templateUrl : 	'views/maker/maker.html',
            controller : 	'makerFormController',
            controllerAs : 'c',
            activeUrl : 	'/form/maker',
            authenticate : true
        })
    ;



    $urlRouterProvider
        .when('/', '/home', function () {
            console.log("/");
        })
        .when('', '/home', function () {
            console.log("nothing");
        })
} ]);