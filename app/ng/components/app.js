var app = angular.module('app', ['ui.router', 'selectize', 'ui.bootstrap-slider']).run();

app.run(['$rootScope', '$location', 'AuthService', '$state', function($rootScope, $location, AuthService, $state){

    // Authentication observer for every state
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState){
        if (toState.authenticate && !AuthService.isLoggedIn){
            // Auth is required on this state but user is not logged in
            event.preventDefault(); // prevent that the desired page will be loaded
            $state.transitionTo('login');
        }
    });
}]);