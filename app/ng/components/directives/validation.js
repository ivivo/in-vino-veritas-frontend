app.directive('validated', function ($compile) {

    var log = new L4JS("validated-directive");

    return {
        restrict: 'A',
        link: function (scope,element, attrs) {
            scope.$watch('validationErrors', function(validationErrors){
                // remove all current warnings
                element.parent().find(".validator-message").remove();

                var model = attrs.ngModel; // field name of input field
                var fieldHasError = false;

                for (var e in validationErrors) {
                    var error = validationErrors[e];

                    // find the right error message for this input field
                    if (model.indexOf(error.path) != -1) {
                        element.parent().not('.rating').addClass('has-error'); // highlight the form-group
                        var message = "";

                        // Translate the message into human readable string
                        switch (error.name){
                            case 'CastError':
                                message = 'Bitte nur durch Punkt separierte Zahlen eingeben'
                                break;

                            case 'ValidatorError':
                                switch (error.kind) {
                                    case 'required':
                                        message = 'Pflichtfeld';
                                        break;
                                    case 'user defined':
                                        message = error.message;
                                        break;
                                    default:
                                        message = error.kind;
                                }
                                break;
                            default: message = error.kind;

                        }

                        // append the message to DOM
                        element.parent().append(angular.element('<div class="validator-message">'+message+'</div>'));
                        fieldHasError = true;
                    }
                }
                if (!fieldHasError) {
                    element.parent().removeClass('has-error');
                }
            });
        }
    };
});