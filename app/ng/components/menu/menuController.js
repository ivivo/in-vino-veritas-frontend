app.controller('menuController',
    ['$scope', '$stateParams', '$rootScope', '$location', 'AuthService',
        function ($scope, $stateParams, $rootScope, $location, AuthService) {

            this.isActive = function (curPath) {
                return ($location.path() == curPath);
            };

        }]);