app.controller('loginController',
    [ '$scope', 'AuthService', '$location',
        function($scope, AuthService, $location) {

            var c = this;

            var log = new L4JS("loginController");

            c.errors = [];

            c.submitLogin = function() {
                c.errors = [];
                var fields = c.fields;
                AuthService.login(fields.username, fields.password, function() {
                    $location.path('/home');
                }, function(){
                    fields.username = "";
                    fields.password = "";
                    $('form[name=registerForm]').addClass('has-error');
                });
                
            }


        } ]);