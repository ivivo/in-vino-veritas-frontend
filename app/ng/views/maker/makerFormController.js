app.controller('makerFormController',
    ['$scope', '$stateParams', '$rootScope', 'HttpService', 'AuthService', 'ReviewService',
        function ($scope, $stateParams, $rootScope, HttpService, AuthService, ReviewService) {

            var c = this;
            var log = new L4JS("formController");
            $scope.ReviewService = ReviewService;

            c.maker = {};

            c.maker.region = {
                country: "de"
            }

            function clearErrors() {
                c.errors = null;
                c.success = null;
            }

            /**
             * get wine makers
             */
            HttpService.get({
                url: '/api/makers',
                success: function (data) {
                    if (AuthService.isAdmin) {
                        c.makers = data;        // Admin should get all makers
                    }
                    else {
                        c.maker = data;         // Maker only receives his profile
                    }
                },
                error: function(data) {
                    console.log(data);
                    if (data.message == "Link yourself to a maker first."){
                        c.success = "Zum Abschließen der Registrierung bitte das Profil vervollständigen.";
                    }
                }
            });

            /**
             * submit wine maker
             */
            c.submitMaker = function () {
                clearErrors();
                if (c.maker == undefined || c.maker._id == undefined || c.maker._id == '') {
                    HttpService.post({
                        url: '/api/makers',
                        data: c.maker,
                        error: function (errors) {
                            c.validationErrors = errors;
                            $scope.validationErrors = errors;
                        },
                        success: function (message) {
                            c.success = message;
                            c.validationErrors = {};
                            $scope.validationErrors = {};
                            $('html, body').animate({ scrollTop: 0 }, 'fast');
                        }
                    });
                }
                else {
                    HttpService.put({
                        url: '/api/makers/' + c.maker._id,
                        data: c.maker,
                        error: function (errors) {
                            c.validationErrors = errors;
                            $scope.validationErrors = errors;
                        },
                        success: function (message) {
                            c.success = (message);
                            c.validationErrors = {};
                            $scope.validationErrors = {};
                            $('html, body').animate({ scrollTop: 0 }, 'fast');
                        }
                    });
                }
            };
        }]);
