app.controller('discoverController',
    ['$scope', '$stateParams', '$rootScope', 'HttpService', 'ReviewService', '$timeout', '$filter',
        function ($scope, $stateParams, $rootScope, HttpService, ReviewService, $timeout, $filter) {

            var c = this;
            $scope.ReviewService = ReviewService;

            var log = new L4JS("discoverController");

            var initTag = $stateParams.tag;
            var url = "/api/wines";

            // initial loading function for wines
            function updateWines() {
                log.debug("update wines")
                // if predefined tag is present select wines according to this tag
                if (typeof initTag != 'undefined') {
                    url = "/api/search/" + initTag;
                }
                // get wines
                HttpService.get({
                    url: url,
                    success: function (data) {
                        log.info("set wine types");
                        for (var w in data) {
                            for (var t in data[w].tags) {
                                var tag = data[w].tags[t];
                                if (tag.type == 'TYPE') {
                                    data[w].type = tag.name;
                                    log.debug(data[w].type);
                                    break;
                                }
                            }
                        }
                        c.wines = data;
                    }
                });
            }

            updateWines();

            /**
             * function to summarize reviews per stars per wine
             */
            c.getStarsCount = function (wine, stars) {
                var val = 0;
                for (i in wine.recommendations) {
                    if (wine.recommendations[i].stars == stars) {
                        val += 1;
                    }
                }
                return val;
            };

            c.filterReviews = {};


            /**
             * Orders
             */
            c.increaseOrders = function (wineID) {
                HttpService.put({
                    url: "/api/wines/" + wineID + "/increment"
                });
            };

            /**
             * Toggle functions to show either reviews or maker details
             */

            c.toggleMakerRecommendations = function (wineID, action) {
                var recommendationsC = '#' + wineID + '-recommendations';
                var makerC = '#' + wineID + '-maker-details';
                var orderButton = '#' + wineID + '-order-button';
                if (action == "review-button") {
                    $(makerC).collapse('hide');
                }
                if (action == "order-button") {
                    $(recommendationsC).collapse("hide");
                    if (!($(makerC).hasClass('in'))) {
                        c.increaseOrders(wineID);
                        console.log("Plus 1");
                    }
                }
            };

            /**
             * Submit Reviews
             */
            function clearErrors() {
                c.errors = null;
                c.success = null;
            }

            c.submitReview = function (wine, review) {
                console.log("Review : " + review);
                clearErrors();
                var data = {
                    url: '/api/wines/' + wine._id + '/recommendations',
                    data: review,
                    error: function (errors) {
                        c.validationErrors = errors;
                        $scope.validationErrors = errors;
                    },
                    success: function (message) {
                        c.success = message;
                        $scope.validationErrors = {};
                        c.successAction(wine._id);
                        console.log("Added recommendation to wine with ID: " + wine._id);
                        ReviewService.updateRecommendations(wine);
                    }
                };
                HttpService.post(data);
            };

            c.successAction = function (wineID) {
                var submitForm = '#' + wineID + '-submit-review';
                var submitButton = '#' + wineID + '-submit-review-button';
                var confirmation = '<span class="pull-right bbutton button_red">Bewertung gespeichert</span>';
                ReviewService.review = {};
                ReviewService.toggleReviewsSize(wineID);
                $(submitForm).collapse('hide');
                $(submitButton).replaceWith(confirmation);
                console.log("Success Action");
            };
            
            /**
             * Filter-Section
             */

            var filterAll = "-- Alle --";

            c.regions = [filterAll];
            c.selectedType = "all";

            HttpService.get({
                url: "/api/regions",
                success: function (data) {
                    for (var i in data) {
                        var region = data[i]._id.name;
                        if (region != null && region != "null") {
                            c.regions.push(data[i]._id.name);
                        }
                    }
                    c.region = c.regions[0];
                }
            });

            c.yearSlider = {
                min: 1950,
                max: 2016
            }

            c.priceSlider = {
                min: 1,
                max: 50
            }


            var filterTimeout = $timeout(function(){},0);

            // timeout to limit requests of immediate filtering
            // wine list is just filtered if user does not input information for at least 500 ms
            c.filterWines = function() {
                $timeout.cancel(filterTimeout);
                filterTimeout = $timeout(getFilteredWines, 500);
            }

            function getFilteredWines() {

                // collect all data needed for filtering

                var filter = {};
                filter.price = {
                    min: c.priceSlider.value[0],
                    max: c.priceSlider.value[1]
                }
                filter.year = {
                    min: c.yearSlider.value[0],
                    max: c.yearSlider.value[1]
                }
                filter.region = c.region == filterAll ? null : c.region;
                filter.tags = [];
                for (var t in c.tags) {
                    var tag = c.tags[t];
                    if (tag.selected == true) {
                        filter.tags.push(tag);
                    }
                }

                if (c.selectedType != "all") {
                    filter.tags.push(c.selectedType);
                }

                // get wines
                HttpService.get({
                    url: '/api/search/filter?'+JSON.stringify(filter),
                    success: function (data) {
                        for (var w in data) {
                            for (var t in data[w].tags) {
                                var tag = data[w].tags[t];
                                if (tag.type == 'TYPE') {
                                    data[w].type = tag.name;
                                    log.debug(data[w]);
                                    break;
                                }
                            }
                        }
                        c.wines = data;
                    }
                });
            }

            /**
             * set c.wine.type from a tag of type 'TYPE'
             */
            function setWineType(wine) {
                for (var t in wine.tags) {
                    var tag = wine.tags[t];
                    if (tag.type == 'TYPE') {
                        wine.type = tag.name;
                        return;
                    }
                }
            }

            /**
             * TAGS
             */

            c.tags = [];
            c.selectedTag = "";
            c.selectedTags = [];
            c.dishTags = [];

            HttpService.get({
                url: "/api/tags",
                success: function (data) {
                    for (var i in data) {
                        var tag = data[i]._id;
                        tag.selected = false;
                        if (tag.name == initTag) {
                            tag.selected = true;
                        }
                        c.tags.push(tag);
                        if (tag.type == 'DISH')
                            c.dishTags.push(tag);
                    }
                }
            });

            c.selectizeConfig = {
                create: false,
                valueField: 'name',
                labelField: 'name',
                placeholder: 'Gericht eingeben...',
                searchField: 'name',
                maxOptions: 5,
                openOnFocus: false,
                onChange: function () {
                    $scope.$apply(function () {
                        c.addSelectedTag(c.selectedTag);
                    });
                },
                maxItems: 1
            };

            // helper function to get a tag object by its name
            function getTagByName(tagName) {
                for (var i in c.tags) {
                    if (c.tags[i].name == tagName)
                        return c.tags[i];
                }
            }

            c.addSelectedTag = function (tagName) {
                getTagByName(tagName).selected = true;
                c.filterWines();
                c.selectedTag = "";
            };

            c.removeSelectedTag = function (tagName) {
                getTagByName(tagName).selected = false;
                c.filterWines();
            }

            c.isTagSelected = function (tagName) {
                var tag = getTagByName(tagName);
                return typeof tag == 'undefined' ? false : tag.selected;
            }

        }]);