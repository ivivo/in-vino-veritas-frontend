app.controller('registrationController',
    [ '$scope', 'AuthService', '$state',
        function($scope, AuthService, $state) {

            var c = this;

            var log = new L4JS("registrationController");

            var passwordMatchError = "Passwörter stimmen nicht überein.";
            c.errors = [];
            c.successMessages = [];

            c.submitRegistration = function() {
                c.successMessages = [];
                c.errors = [];
                var fields = c.fields;
                if (fields.password != fields.passwordConfirm) {
                    c.errors.push(passwordMatchError);
                    return;
                }
                AuthService.register(fields.username, fields.password, function(){
                    c.successMessages.push("Sie wurden erfolgreich registriert und können sich nun mit Username und Passwort unter Login anmelden.");
                    $state.transitionTo('formMaker');
                });

            }

        } ]);