app.controller('wineFormController',
    ['$scope', '$stateParams', '$rootScope', 'HttpService', 'AuthService', 'ReviewService',
        function ($scope, $stateParams, $rootScope, HttpService, AuthService, ReviewService) {

            var c = this;
            var log = new L4JS("formController");
            $scope.ReviewService = ReviewService;

            c.wine = {
                tags: []
            };
            c.newWine = {
                tags: []
            };
            c.maker = {};
            c.tag = {};
            c.tags = [];

            c.maker.region = {
                country: "de"
            }

            setWineType(c.wine);

            function clearErrors() {
                c.errors = null;
                c.success = null;
            }

            /**
             * get wine makers
             */
            HttpService.get({
                url: '/api/makers',
                success: function (data) {
                    if (AuthService.isAdmin) {
                        c.makers = data;        // Admin should get all makers
                    }
                    else {
                        c.maker = data;         // Maker only receives his profile
                    }
                },
                error: function (data) {
                    console.log(data);
                    if (data.message == "Link yourself to a maker first.") {
                        c.success = "Zum Abschließen der Registrierung bitte das Profil vervollständigen.";
                    }
                }
            });

            /**
             * get wines
             */
            // URL as Admin
            var winesUrl = "/api/own/wines";
            if (AuthService.isAdmin) {
                winesUrl = "/api/wines";
            }


            function getWines() {
                var currentWineName = c.wine.name;
                HttpService.get({
                    url: winesUrl,
                    success: function (data) {
                        c.wines = data;
                        for (var w in c.wines) {
                            setWineType(c.wines[w]);
                            if (c.wines[w].name == currentWineName) {
                                c.wine = c.wines[w];
                                if (typeof c.wine.tags == 'undefined') {
                                    c.wine.tags = [];
                                }
                            }
                            if (typeof c.wine == 'undefined') {
                                c.wine = {
                                    tags: []
                                };
                                setWineType(c.wine);
                            }
                        }
                    }
                });
            }

            getWines();

            HttpService.get({
                url: '/api/tags',
                success: function (data) {
                    for (var i in data) {
                        c.tags.push(data[i]._id);
                    }
                }
            });

            /**
             * submit wine to backend
             */
            c.submitWine = function () {
                clearErrors();
                setWineTypeAsTag();

                c.wine.maker = c.maker._id;
                var data = {
                    url: '/api/wines/' + c.wine._id,
                    data: c.wine,
                    error: function (errors) {
                        c.validationErrors = errors;
                        $scope.validationErrors = errors;
                        setWineType(c.wine);
                    },
                    success: function (message) {
                        c.success = message;
                        $scope.validationErrors = {};
                        getWines();
                        $('html, body').animate({ scrollTop: 0 }, 'fast');
                    }
                };
                if (typeof c.wine._id == 'undefined' || c.wine._id == '') {
                    data.url = '/api/wines';
                    HttpService.post(data);
                } else {
                    HttpService.put(data);
                }
            }

            c.deleteWine = function () {
                var data = {
                    url: '/api/wines/' + c.wine._id,
                    error: function (errors) {
                        c.errors = errors;
                    },
                    success: function (message) {
                        c.success = message;
                        c.wine = {};
                        $('html, body').animate({ scrollTop: 0 }, 'fast');
                        getWines();
                    }
                };
                HttpService.delete(data);
            }

            function newTag() {
                return {
                    name: '',
                    type: ''
                };
            }

            c.addTag = function (tag) {
                if (typeof tag == 'undefined')
                    c.wine.tags.push(newTag());
                else
                    c.wine.tags.push(tag);
            }

            c.addTagOfType = function (type) {
                if (typeof type != 'undefined') {
                    var newTag = {
                        name: '',
                        type: ''
                    };
                    newTag.type = type;
                    c.wine.tags.push(newTag);
                }
            }

            c.removeTag = function (tag) {
                for (var t in c.wine.tags) {
                    var that = c.wine.tags[t];
                    if (that.name == tag.name && that.type == tag.type) {
                        c.wine.tags.splice(t, 1);
                        return;
                    }
                }
            }

            function checkWine() {
                if (typeof c.wine == 'undefined' || c.wine == null) {
                    c.wine = {
                        tags: [],
                        type: "Rotwein"
                    }
                }
            }

            /**
             * set tag 'TYPE' from selected value in select as a tag
             */
            function setWineTypeAsTag() {
                checkWine();
                var i = c.wine.tags.length;
                while (i--) {
                    var tag = c.wine.tags[i];
                    if (tag.type == 'TYPE') {
                        c.wine.tags.splice(i, 1);
                    }
                }
                c.wine.tags.push({
                    type: 'TYPE',
                    name: c.wine.type
                });
            }

            /**
             * set c.wine.type from a tag of type 'TYPE'
             */
            function setWineType(wine) {
                for (var t in wine.tags) {
                    var tag = wine.tags[t];
                    if (tag.type == 'TYPE') {
                        wine.type = tag.name;
                        return;
                    }
                }
                wine.type = "Rotwein";
            }

        }]);
