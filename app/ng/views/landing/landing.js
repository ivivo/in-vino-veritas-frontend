app.controller('landingController',
    ['$scope', 'HttpService', '$state',
        function ($scope, HttpService, $state) {

            c = this;

            var log = new L4JS("landingController");

            c.tags = [];

            HttpService.get({
                url: "/api/tags",
                success: function(data) {
                    for (var i in data) {
                        if (data[i]._id.type == 'DISH')
                            c.tags.push(data[i]._id);
                    }
                }
            });

            c.goDiscover = function() {
                if (typeof c.selectedTag == 'undefined')
                    return;
                $state.go("discover", {tag:c.selectedTag});
            }

            c.selectedTag;

            c.selectizeConfig = {
                create: false,
                valueField: 'name',
                labelField: 'name',
                placeholder: 'Pick something',
                searchField: 'name',
                maxOptions: 5,
                openOnFocus: false,
                onChange: function(selectize){
                    log.debug(c.selectedTag);
                    $state.go("discoverTag", {tag:c.selectedTag});
                },
                maxItems: 1
            };

        }]);